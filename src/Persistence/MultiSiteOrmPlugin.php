<?php declare(strict_types=1);

namespace Dms\Package\MultiSite\Persistence;

use Dms\Core\Persistence\Db\Mapping\Definition\MapperDefinition;
use Dms\Core\Persistence\Db\Mapping\EntityMapper;
use Dms\Core\Persistence\Db\Mapping\IObjectMapper;
use Dms\Core\Persistence\Db\Mapping\Plugin\IOrmPlugin;
use Dms\Core\Persistence\Db\Query\Expression\Expr;
use Dms\Core\Persistence\Db\Query\Select;
use Dms\Package\MultiSite\Core\Config\MultiSiteConfiguration;
use Dms\Package\MultiSite\Core\Resolver\CurrentWebsiteResolver;
use Dms\Package\MultiSite\Core\Website;

/**
 * @author Elliot Levin <elliotlevin@hotmail.com>
 */
class MultiSiteOrmPlugin implements IOrmPlugin
{

    /**
     * @var CurrentWebsiteResolver
     */
    protected $currentWebsiteResolver;

    /**
     * @var MultiSiteConfiguration
     */
    protected $configuration;

    /**
     * MultiSiteOrmPlugin constructor.
     *
     * @param CurrentWebsiteResolver $currentWebsiteResolver
     * @param MultiSiteConfiguration $configuration
     */
    public function __construct(CurrentWebsiteResolver $currentWebsiteResolver, MultiSiteConfiguration $configuration)
    {
        $this->currentWebsiteResolver = $currentWebsiteResolver;
        $this->configuration          = $configuration;
    }


    protected function isWebsiteSpecificEntity(string $class): bool
    {
        if (is_a($class, Website::class, true)) {
            return false;
        }

        foreach ($this->configuration->getDisabledEntityClasses() as $disabledEntityClass) {
            if (is_a($class, $disabledEntityClass, true)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Hook for the defining an object mapper.
     *
     * @param IObjectMapper    $mapper
     * @param MapperDefinition $map
     *
     * @return void
     */
    public function defineMapper(IObjectMapper $mapper, MapperDefinition $map)
    {
        if (!($mapper instanceof EntityMapper)) {
            return;
        }

        if (!$this->isWebsiteSpecificEntity($map->getObjectType())) {
            return;
        }

        $foreignKeyName = $this->configuration->getWebsiteForeignKeyName();
        $map->column($foreignKeyName)->asUnsignedInt();

        $map->accessorRelation(
            function () {
                return $this->currentWebsiteResolver->resolveCurrentWebsite()->getId();
            },
            function () {
                // Unused
            }
        )->to($this->configuration->getWebsiteClass())
            ->manyToOneId()
            ->onDeleteDoNothing()
            ->withRelatedIdAs($foreignKeyName);

        $map->appendColumnToUniqueIndexes($foreignKeyName);
    }

    /**
     * Hook for loading the SELECT query from the supplied object mapper.
     *
     * @param IObjectMapper $mapper
     * @param Select        $select
     *
     * @return void
     */
    public function loadSelect(IObjectMapper $mapper, Select $select)
    {
        if (!($mapper instanceof EntityMapper)) {
            return;
        }

        if (!$this->isWebsiteSpecificEntity($mapper->getObjectType())) {
            return;
        }

        $foreignKeyName = $this->configuration->getWebsiteForeignKeyName();

        if ($select->getTable()->hasColumn($foreignKeyName)) {
            $column = $select->getTable()->getColumn($foreignKeyName);

            $select->where(Expr::equal(
                Expr::column($select->getTableAlias(), $column),
                Expr::param($column->getType(), $this->currentWebsiteResolver->resolveCurrentWebsite()->getId())
            ));
        }
    }
}