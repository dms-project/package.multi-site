<?php declare(strict_types = 1);

namespace Dms\Package\MultiSite\Cms;

use Dms\Core\Exception\NotImplementedException;
use Dms\Core\ICms;
use Dms\Core\Ioc\IIocContainer;
use Dms\Core\Package\Package;
use Dms\Package\MultiSite\Core\Config\MultiSiteConfiguration;
use Dms\Package\MultiSite\Core\Config\MultiSiteConfigurationBuilder;
use Dms\Package\MultiSite\Core\Resolver\CurrentWebsiteResolver;

/**
 * @author Elliot Levin <elliotlevin@hotmail.com>
 */
abstract class MultiSitePackage extends Package
{
    /**
     * Boots and configures the package resources and services.
     *
     * @param ICms $cms
     *
     * @return void
     */
    public static function boot(ICms $cms)
    {
        static::bootIocContainer($cms->getIocContainer());
    }

    /**
     * Boots and configures the package resources and services.
     *
     * @param IIocContainer $iocContainer
     */
    public static function bootIocContainer(IIocContainer $iocContainer)
    {
        $iocContainer->bind(IIocContainer::SCOPE_SINGLETON, CurrentWebsiteResolver::class, CurrentWebsiteResolver::class);

        $iocContainer->bindCallback(
            IIocContainer::SCOPE_SINGLETON,
            MultiSiteConfiguration::class,
            function () : MultiSiteConfiguration {
                $builder = MultiSiteConfiguration::builder();

                static::defineConfig($builder);

                return $builder->build();
            }
        );
    }

    /**
     * Defines the config of the multi-site package.
     *
     * @param MultiSiteConfigurationBuilder $config
     *
     * @return void
     * @throws NotImplementedException
     */
    protected static function defineConfig(MultiSiteConfigurationBuilder $config)
    {
        throw NotImplementedException::format(
            'Invalid multi-site package class %s: the \'%s\' static method must be overridden',
            get_called_class(), __FUNCTION__
        );
    }
}