<?php declare(strict_types = 1);

namespace Dms\Package\MultiSite\Core\Config;

/**
 * The multi-site configuration builder class.
 *
 * @author Elliot Levin <elliotlevin@hotmail.com>
 */
class MultiSiteConfigurationBuilder
{
    /**
     * @var string
     */
    protected $websiteClass;

    /**
     * @var string
     */
    protected $websiteForeignKeyName;

    /**
     * @var string
     */
    protected $websiteRepositoryClass;

    /**
     * @var callable
     */
    protected $currentDomainNameResolver;

    /**
     * @var string[]
     */
    protected $disabledEntityClasses = [];

    /**
     * @param string $websiteClass
     *
     * @return MultiSiteConfigurationBuilder
     */
    public function setWebsiteClass(string $websiteClass) : self
    {
        $this->websiteClass = $websiteClass;

        return $this;
    }

    /**
     * @param string $websiteForeignKeyName
     *
     * @return MultiSiteConfigurationBuilder
     */
    public function setWebsiteForeignKeyName(string $websiteForeignKeyName) : self
    {
        $this->websiteForeignKeyName = $websiteForeignKeyName;

        return $this;
    }

    /**
     * @param string $websiteRepositoryClass
     *
     * @return MultiSiteConfigurationBuilder
     */
    public function setWebsiteRepositoryClass(string $websiteRepositoryClass) : self
    {
        $this->websiteRepositoryClass = $websiteRepositoryClass;

        return $this;
    }

    /**
     * @param callable $currentDomainNameResolver
     *
     * @return MultiSiteConfigurationBuilder
     */
    public function setCurrentDomainNameResolver(callable $currentDomainNameResolver) : self
    {
        $this->currentDomainNameResolver = $currentDomainNameResolver;

        return $this;
    }

    /**
     * @param \string[] $disabledEntityClasses
     *
     * @return MultiSiteConfigurationBuilder
     */
    public function setDisabledEntityClasses(array $disabledEntityClasses) : self
    {
        $this->disabledEntityClasses = $disabledEntityClasses;

        return $this;
    }

    /**
     * @return MultiSiteConfiguration
     */
    public function build() : MultiSiteConfiguration
    {
        return new MultiSiteConfiguration(
            $this->websiteClass,
            $this->websiteForeignKeyName,
            $this->websiteRepositoryClass,
            $this->currentDomainNameResolver,
            $this->disabledEntityClasses
        );
    }
}