<?php declare(strict_types = 1);

namespace Dms\Package\MultiSite\Core\Config;

use Dms\Core\Exception\InvalidArgumentException;
use Dms\Core\Persistence\IRepository;
use Dms\Package\MultiSite\Core\Website;

/**
 * The multi-site configuration class.
 *
 * @author Elliot Levin <elliotlevin@hotmail.com>
 */
class MultiSiteConfiguration
{
    /**
     * @var string
     */
    protected $websiteClass;

    /**
     * @var string
     */
    protected $websiteForeignKeyName;

    /**
     * @var string
     */
    protected $websiteRepositoryClass;

    /**
     * @var callable
     */
    protected $currentDomainNameResolver;

    /**
     * @var string[]
     */
    protected $disabledEntityClasses;

    /**
     * MultiSiteConfiguration constructor.
     *
     * @param string   $websiteClass
     * @param string   $websiteForeignKeyName
     * @param string   $websiteRepositoryClass
     * @param callable $currentDomainNameResolver
     * @param string[] $disabledEntityClasses
     */
    public function __construct(
        string $websiteClass,
        string $websiteForeignKeyName,
        string $websiteRepositoryClass,
        callable $currentDomainNameResolver,
        array $disabledEntityClasses
    ) {
        InvalidArgumentException::verify(
            is_a($websiteClass, Website::class, true),
            'The website class must be of type %s, %s given',
            Website::class, $websiteClass
        );

        InvalidArgumentException::verify(
            is_a($websiteRepositoryClass, IRepository::class, true),
            'The website repository class must be of type %s, %s given',
            IRepository::class, $websiteRepositoryClass
        );

        $this->websiteClass              = $websiteClass;
        $this->websiteForeignKeyName     = $websiteForeignKeyName;
        $this->websiteRepositoryClass    = $websiteRepositoryClass;
        $this->currentDomainNameResolver = $currentDomainNameResolver;
        $this->disabledEntityClasses     = $disabledEntityClasses;
    }

    /**
     * @return MultiSiteConfigurationBuilder
     */
    public static function builder() : MultiSiteConfigurationBuilder
    {
        return new MultiSiteConfigurationBuilder();
    }

    /**
     * @return string
     */
    public function getWebsiteClass() : string
    {
        return $this->websiteClass;
    }

    /**
     * @return string
     */
    public function getWebsiteRepositoryClass() : string
    {
        return $this->websiteRepositoryClass;
    }

    /**
     * @return string
     */
    public function getWebsiteForeignKeyName() : string
    {
        return $this->websiteForeignKeyName;
    }

    /**
     * @return callable
     */
    public function getCurrentDomainNameResolver() : callable
    {
        return $this->currentDomainNameResolver;
    }

    /**
     * @return string[]
     */
    public function getDisabledEntityClasses() : array
    {
        return $this->disabledEntityClasses;
    }
}