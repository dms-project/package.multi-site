<?php declare(strict_types = 1);

namespace Dms\Package\MultiSite\Core;

use Dms\Core\Model\Object\ClassDefinition;
use Dms\Core\Model\Object\Entity;

/**
 * The website entity base class.
 *
 * @author Elliot Levin <elliotlevin@hotmail.com>
 */
abstract class Website extends Entity
{
    const NAME = 'name';
    const DOMAIN_NAME = 'domainName';

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $domainName;

    /**
     * Website constructor.
     *
     * @param string $name
     * @param string $domainName
     */
    public function __construct(string $name, string $domainName)
    {
        parent::__construct();
        $this->name       = $name;
        $this->domainName = $domainName;
    }

    /**
     * Defines the structure of this entity.
     *
     * @param ClassDefinition $class
     */
    final protected function defineEntity(ClassDefinition $class)
    {
        $class->property($this->name)->asString();

        $class->property($this->domainName)->asString();

        $this->defineWebsite($class);
    }

    /**
     * Defines the structure of this entity.
     *
     * @param ClassDefinition $class
     */
    abstract protected function defineWebsite(ClassDefinition $class);
}