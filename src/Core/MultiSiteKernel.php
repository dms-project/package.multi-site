<?php declare(strict_types = 1);

namespace Dms\Package\MultiSite\Core;

use Dms\Core\Ioc\IIocContainer;
use Dms\Package\MultiSite\Core\Resolver\CurrentWebsiteResolver;

/**
 * The multi-site kernel class.
 *
 * @author Elliot Levin <elliotlevin@hotmail.com>
 */
class MultiSiteKernel
{
    /**
     * @var IIocContainer
     */
    protected $iocContainer;

    /**
     * MultiSiteKernel constructor.
     *
     * @param IIocContainer $iocContainer
     */
    public function __construct(IIocContainer $iocContainer)
    {
        $this->iocContainer = $iocContainer;
    }

    /**
     * Gets the current website.
     *
     * @return Website
     * @throws UnknownDomainException
     */
    public function getCurrentWebsite() : Website
    {
        /** @var CurrentWebsiteResolver $websiteResolver */
        $websiteResolver = $this->iocContainer->get(CurrentWebsiteResolver::class);

        return $websiteResolver->resolveCurrentWebsite();
    }
}