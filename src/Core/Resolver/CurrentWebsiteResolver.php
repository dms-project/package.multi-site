<?php declare(strict_types = 1);

namespace Dms\Package\MultiSite\Core\Resolver;

use Dms\Core\Ioc\IIocContainer;
use Dms\Core\Persistence\IRepository;
use Dms\Package\MultiSite\Core\Config\MultiSiteConfiguration;
use Dms\Package\MultiSite\Core\UnknownDomainException;
use Dms\Package\MultiSite\Core\Website;

/**
 * @author Elliot Levin <elliotlevin@hotmail.com>
 */
class CurrentWebsiteResolver
{
    /**
     * @var MultiSiteConfiguration
     */
    protected $configuration;

    /**
     * @var IIocContainer
     */
    protected $iocContainer;

    /**
     * @var Website[]
     */
    protected $websiteCache = [];

    /**
     * CurrentWebsiteResolver constructor.
     *
     * @param MultiSiteConfiguration $configuration
     * @param IIocContainer          $iocContainer
     */
    public function __construct(MultiSiteConfiguration $configuration, IIocContainer $iocContainer)
    {
        $this->configuration = $configuration;
        $this->iocContainer  = $iocContainer;
    }

    /**
     * Resolves the current website.
     *
     * @return Website
     * @throws UnknownDomainException
     */
    public function resolveCurrentWebsite() : Website
    {
        $domainName = call_user_func($this->configuration->getCurrentDomainNameResolver());

        if (!isset($this->websiteCache[$domainName])) {

            /** @var IRepository $websiteRepository */
            $websiteRepository = $this->iocContainer->get($this->configuration->getWebsiteRepositoryClass());

            $websites = $websiteRepository->matching(
                $websiteRepository->criteria()
                    ->where(Website::DOMAIN_NAME, '=', $domainName)
            );

            if (!$websites) {
                throw UnknownDomainException::format(
                    'Could not find matching website for \'%s\' domain', $domainName
                );
            }

            $this->websiteCache[$domainName] = reset($websites);
        }

        return $this->websiteCache[$domainName];
    }
}