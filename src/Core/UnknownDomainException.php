<?php declare(strict_types = 1);

namespace Dms\Package\MultiSite\Core;

use Dms\Core\Exception\BaseException;

/**
 * The unknown website exception.
 *
 * @author Elliot Levin <elliotlevin@hotmail.com>
 */
class UnknownDomainException extends BaseException
{

}