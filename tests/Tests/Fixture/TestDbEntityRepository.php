<?php declare(strict_types = 1);

namespace Dms\Package\MultiSite\Tests\Fixture;

use Dms\Core\Persistence\Db\Connection\IConnection;
use Dms\Core\Persistence\Db\Mapping\IOrm;
use Dms\Core\Persistence\DbRepository;

/**
 *
 * @author Elliot Levin <elliotlevin@hotmail.com>
 */
class TestDbEntityRepository extends DbRepository
{
    /**
     * TestDbEntityRepository constructor.
     *
     * @param IConnection $connection
     * @param IOrm        $orm
     */
    public function __construct(IConnection $connection, IOrm $orm)
    {
        parent::__construct($connection, $orm->getEntityMapper(TestEntity::class));
    }
}