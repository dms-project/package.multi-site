<?php declare(strict_types = 1);

namespace Dms\Package\MultiSite\Tests\Fixture;
use Dms\Core\Model\Object\ClassDefinition;
use Dms\Core\Model\Object\Entity;

/**
 * @author Elliot Levin <elliotlevin@hotmail.com>
 */ 
class TestEntity extends Entity
{
    const UNIQUE_PROPERTY = 'uniqueProperty';
    
    /**
     * @var string
     */
    public $uniqueProperty;

    /**
     * TestEntity constructor.
     *
     * @param string $uniqueProperty
     */
    public function __construct($id, string $uniqueProperty)
    {
        parent::__construct($id);
        $this->uniqueProperty = $uniqueProperty;
    }


    /**
     * Defines the structure of this entity.
     *
     * @param ClassDefinition $class
     */
    protected function defineEntity(ClassDefinition $class)
    {
        $class->property($this->uniqueProperty)->asString();
    }
}