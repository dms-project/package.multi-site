<?php declare(strict_types = 1);

namespace Dms\Package\MultiSite\Tests\Fixture;

use Dms\Core\Persistence\Db\Mapping\Definition\Orm\OrmDefinition;
use Dms\Core\Persistence\Db\Mapping\Orm;
use Dms\Package\MultiSite\Persistence\MultiSiteOrmPlugin;

/**
 *
 * @author Elliot Levin <elliotlevin@hotmail.com>
 */
class TestMultiSiteOrm extends Orm
{

    /**
     * Defines the object mappers registered in the orm.
     *
     * @param OrmDefinition $orm
     *
     * @return void
     */
    protected function define(OrmDefinition $orm)
    {
        $orm->entities([
            TestEntity::class         => TestEntityMapper::class,
            TestDisabledEntity::class => TestDisabledEntityMapper::class,
            TestWebsite::class        => TestWebsiteMapper::class,
        ]);

        $orm->registerPlugin(MultiSiteOrmPlugin::class);
    }
}