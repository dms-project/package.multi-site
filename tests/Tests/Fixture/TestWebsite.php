<?php declare(strict_types = 1);

namespace Dms\Package\MultiSite\Tests\Fixture;
use Dms\Core\Model\Object\ClassDefinition;
use Dms\Package\MultiSite\Core\Website;

/**
 * @author Elliot Levin <elliotlevin@hotmail.com>
 */
class TestWebsite extends Website
{

    /**
     * Defines the structure of this entity.
     *
     * @param ClassDefinition $class
     */
    protected function defineWebsite(ClassDefinition $class)
    {

    }
}