<?php declare(strict_types = 1);

namespace Dms\Package\MultiSite\Tests\Fixture;
use Dms\Core\Persistence\Db\Mapping\Definition\MapperDefinition;
use Dms\Core\Persistence\Db\Mapping\EntityMapper;

/**
 *
 * @author Elliot Levin <elliotlevin@hotmail.com>
 */
class TestWebsiteMapper extends EntityMapper
{

    /**
     * Defines the entity mapper
     *
     * @param MapperDefinition $map
     *
     * @return void
     */
    protected function define(MapperDefinition $map)
    {
        $map->type(TestWebsite::class);
        $map->toTable('websites');

        $map->idToPrimaryKey('id');

        $map->property(TestWebsite::NAME)->to('name')->asVarchar(255);
        $map->property(TestWebsite::DOMAIN_NAME)->to('domain_name')->unique()->asVarchar(255);
    }
}