<?php declare(strict_types = 1);

namespace Dms\Package\MultiSite\Tests\Fixture;

use Dms\Core\Package\Definition\PackageDefinition;
use Dms\Package\MultiSite\Cms\MultiSitePackage;
use Dms\Package\MultiSite\Core\Config\MultiSiteConfigurationBuilder;

/**
 *
 * @author Elliot Levin <elliotlevin@hotmail.com>
 */
class TestMultiSitePackage extends MultiSitePackage
{
    /**
     * @var string
     */
    public static $currentDomain;

    /**
     * Defines the config of the multi-site package.
     *
     * @param MultiSiteConfigurationBuilder $config
     *
     * @return void
     * @throws NotImplementedException
     */
    protected static function defineConfig(MultiSiteConfigurationBuilder $config)
    {
        $config
            ->setCurrentDomainNameResolver(function () {
                return self::$currentDomain;
            })
            ->setWebsiteClass(TestWebsite::class)
            ->setWebsiteForeignKeyName('website_id')
            ->setWebsiteRepositoryClass(TestDbWebsiteRepository::class)
            ->setDisabledEntityClasses([
                TestDisabledEntity::class,
            ]);
    }

    /**
     * Defines the structure of this cms package.
     *
     * @param PackageDefinition $package
     *
     * @return void
     */
    protected function define(PackageDefinition $package)
    {
        $package->name('multi-site');

        $package->modules([
            'websites' => TestWebsiteModule::class,
        ]);
    }
}