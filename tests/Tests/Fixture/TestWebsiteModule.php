<?php declare(strict_types = 1);

namespace Dms\Package\MultiSite\Tests\Fixture;
use Dms\Common\Structure\Field;
use Dms\Core\Auth\IAuthSystem;
use Dms\Core\Common\Crud\CrudModule;
use Dms\Core\Common\Crud\Definition\CrudModuleDefinition;
use Dms\Core\Common\Crud\Definition\Form\CrudFormDefinition;
use Dms\Core\Model\IMutableObjectSet;

/**
 *
 * @author Elliot Levin <elliotlevin@hotmail.com>
 */
class TestWebsiteModule extends CrudModule
{
    /**
     * @inheritDoc
     */
    public function __construct(IMutableObjectSet $dataSource, IAuthSystem $authSystem)
    {
        parent::__construct($dataSource, $authSystem);
    }

    /**
     * Defines the structure of this module.
     *
     * @param CrudModuleDefinition $module
     */
    protected function defineCrudModule(CrudModuleDefinition $module)
    {
        $module->name('websites');

        $module->crudForm(function (CrudFormDefinition $form) {
            $form->section('Details', [
                $form->field(
                    Field::create('name', 'Name')->string()->required()
                )->bindToProperty(TestWebsite::NAME),
                //
                $form->field(
                    Field::create('domain', 'Domain')->string()->required()
                )->bindToProperty(TestWebsite::DOMAIN_NAME),
            ]);
        });
    }
}