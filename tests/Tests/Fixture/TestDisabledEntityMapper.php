<?php declare(strict_types = 1);

namespace Dms\Package\MultiSite\Tests\Fixture;

use Dms\Core\Persistence\Db\Mapping\Definition\MapperDefinition;
use Dms\Core\Persistence\Db\Mapping\EntityMapper;

/**
 * @author Elliot Levin <elliotlevin@hotmail.com>
 */
class TestDisabledEntityMapper extends EntityMapper
{

    /**
     * Defines the entity mapper
     *
     * @param MapperDefinition $map
     *
     * @return void
     */
    protected function define(MapperDefinition $map)
    {
        $map->type(TestDisabledEntity::class);
        $map->toTable('disabled_entities');

        $map->idToPrimaryKey('id');
    }
}