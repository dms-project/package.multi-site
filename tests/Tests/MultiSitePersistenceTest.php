<?php declare(strict_types = 1);

namespace Dms\Package\MultiSite\Tests;

use Dms\Core\Ioc\IIocContainer;
use Dms\Core\Persistence\Db\Connection\IConnection;
use Dms\Core\Persistence\Db\Mapping\IOrm;
use Dms\Core\Persistence\Db\Query\Expression\Expr;
use Dms\Core\Persistence\Db\Query\Select;
use Dms\Core\Persistence\Db\Schema\Column;
use Dms\Core\Persistence\Db\Schema\Database;
use Dms\Core\Persistence\Db\Schema\ForeignKey;
use Dms\Core\Persistence\Db\Schema\ForeignKeyMode;
use Dms\Core\Persistence\Db\Schema\Index;
use Dms\Core\Persistence\Db\Schema\PrimaryKeyBuilder;
use Dms\Core\Persistence\Db\Schema\Table;
use Dms\Core\Persistence\Db\Schema\Type\Varchar;
use Dms\Core\Tests\Persistence\Db\Integration\Mapping\DbIntegrationTest;
use Dms\Library\Testing\Helper\TestIocContainer;
use Dms\Package\MultiSite\Core\UnknownDomainException;
use Dms\Package\MultiSite\Tests\Fixture\TestDbDisabledEntityRepository;
use Dms\Package\MultiSite\Tests\Fixture\TestDbEntityRepository;
use Dms\Package\MultiSite\Tests\Fixture\TestDisabledEntity;
use Dms\Package\MultiSite\Tests\Fixture\TestEntity;
use Dms\Package\MultiSite\Tests\Fixture\TestMultiSiteOrm;
use Dms\Package\MultiSite\Tests\Fixture\TestMultiSitePackage;

/**
 *
 * @author Elliot Levin <elliotlevin@hotmail.com>
 */
class MultiSitePersistenceTest extends DbIntegrationTest
{
    /**
     * @var IIocContainer
     */
    protected $testIocContainer;

    /**
     * @return IOrm
     */
    protected function loadOrm()
    {
        $this->testIocContainer = new TestIocContainer();

        TestMultiSitePackage::bootIocContainer($this->testIocContainer);

        $orm = new TestMultiSiteOrm($this->testIocContainer);
        $this->testIocContainer->bindValue(IOrm::class, $orm);

        return $orm;
    }

    public function setUp()
    {
        parent::setUp();

        $this->testIocContainer->bindValue(IConnection::class, $this->connection);

        $this->repo = new TestDbEntityRepository($this->connection, $this->orm);

        $this->setDataInDb([
            'websites' => [
                ['id' => 1, 'name' => 'A', 'domain_name' => 'one.com'],
                ['id' => 2, 'name' => 'B', 'domain_name' => 'two.com'],
                ['id' => 3, 'name' => 'C', 'domain_name' => 'three.com'],
            ],
        ]);
    }

    public function testBuildsCorrectDbStructure()
    {
        $expected = new Database([
            new Table(
                'websites',
                [
                    PrimaryKeyBuilder::incrementingInt('id'),
                    new Column('name', new Varchar(255)),
                    new Column('domain_name', new Varchar(255)),
                ],
                [new Index('websites_domain_name_unique_index', true, ['domain_name'])]
            ),
            new Table(
                'entities',
                [
                    PrimaryKeyBuilder::incrementingInt('id'),
                    new Column('unique_column', new Varchar(255)),
                    new Column('website_id', PrimaryKeyBuilder::primaryKeyType()),
                ],
                [new Index('entities_unique_column_unique_index', true, ['unique_column', 'website_id'])],
                [new ForeignKey('fk_entities_website_id_websites', ['website_id'], 'websites', ['id'], ForeignKeyMode::DO_NOTHING, ForeignKeyMode::CASCADE)]
            ),
            new Table(
                'disabled_entities',
                [
                    PrimaryKeyBuilder::incrementingInt('id'),
                ]
            ),
        ]);

        $this->assertEquals($expected, $this->orm->getDatabase());
    }

    public function testPersistToFirstDomain()
    {
        TestMultiSitePackage::$currentDomain = 'one.com';

        $this->repo->save(new TestEntity(null, 'a'));
        $this->repo->save(new TestEntity(null, 'b'));

        $this->assertDatabaseDataSameAs([
            'websites' => [
                ['id' => 1, 'name' => 'A', 'domain_name' => 'one.com'],
                ['id' => 2, 'name' => 'B', 'domain_name' => 'two.com'],
                ['id' => 3, 'name' => 'C', 'domain_name' => 'three.com'],
            ],
            'entities' => [
                ['id' => 1, 'unique_column' => 'a', 'website_id' => 1],
                ['id' => 2, 'unique_column' => 'b', 'website_id' => 1],
            ],
        ]);
    }

    public function testPersistToMultipleDomains()
    {
        TestMultiSitePackage::$currentDomain = 'one.com';
        $this->repo->save(new TestEntity(null, 'a'));
        TestMultiSitePackage::$currentDomain = 'three.com';
        $this->repo->save(new TestEntity(null, 'a'));

        $this->assertDatabaseDataSameAs([
            'websites' => [
                ['id' => 1, 'name' => 'A', 'domain_name' => 'one.com'],
                ['id' => 2, 'name' => 'B', 'domain_name' => 'two.com'],
                ['id' => 3, 'name' => 'C', 'domain_name' => 'three.com'],
            ],
            'entities' => [
                ['id' => 1, 'unique_column' => 'a', 'website_id' => 1],
                ['id' => 2, 'unique_column' => 'a', 'website_id' => 3],
            ],
        ]);
    }

    public function testPersistToUnknownDomain()
    {
        TestMultiSitePackage::$currentDomain = 'some-invalid-domain.com';

        $this->expectException(UnknownDomainException::class);
        $this->repo->save(new TestEntity(null, 'test'));
    }

    public function testLoadFromFirstDomain()
    {
        TestMultiSitePackage::$currentDomain = 'one.com';

        $this->setDataInDb([
            'websites' => [
                ['id' => 1, 'name' => 'A', 'domain_name' => 'one.com'],
                ['id' => 2, 'name' => 'B', 'domain_name' => 'two.com'],
                ['id' => 3, 'name' => 'C', 'domain_name' => 'three.com'],
            ],
            'entities' => [
                ['id' => 1, 'unique_column' => 'a', 'website_id' => 1],
                ['id' => 2, 'unique_column' => 'b', 'website_id' => 1],
                ['id' => 3, 'unique_column' => 'a', 'website_id' => 2],
                ['id' => 4, 'unique_column' => 'c', 'website_id' => 3],
                ['id' => 5, 'unique_column' => 'd', 'website_id' => 3],
            ],
        ]);

        $this->assertEquals([
            new TestEntity(1, 'a'),
            new TestEntity(2, 'b'),
        ], $this->repo->getAll());

        $this->assertExecutedQueries([
            Select::allFrom($this->getSchemaTable('websites'))
                ->where(Expr::equal(
                    Expr::tableColumn($this->getSchemaTable('websites'), 'domain_name'),
                    Expr::param(null, 'one.com')
                )),
            //
            Select::allFrom($this->getSchemaTable('entities'))
                ->where(Expr::equal(
                    Expr::tableColumn($this->getSchemaTable('entities'), 'website_id'),
                    Expr::param($this->getSchemaTable('entities')->getColumn('website_id')->getType(), 1)
                )),
        ]);
    }

    public function testLoadFromMultipleDomain()
    {

        $this->setDataInDb([
            'websites' => [
                ['id' => 1, 'name' => 'A', 'domain_name' => 'one.com'],
                ['id' => 2, 'name' => 'B', 'domain_name' => 'two.com'],
                ['id' => 3, 'name' => 'C', 'domain_name' => 'three.com'],
            ],
            'entities' => [
                ['id' => 1, 'unique_column' => 'a', 'website_id' => 1],
                ['id' => 2, 'unique_column' => 'b', 'website_id' => 1],
                ['id' => 3, 'unique_column' => 'a', 'website_id' => 2],
                ['id' => 4, 'unique_column' => 'c', 'website_id' => 3],
                ['id' => 5, 'unique_column' => 'd', 'website_id' => 3],
            ],
        ]);

        TestMultiSitePackage::$currentDomain = 'one.com';
        $this->assertEquals([
            new TestEntity(1, 'a'),
            new TestEntity(2, 'b'),
        ], $this->repo->getAll());


        TestMultiSitePackage::$currentDomain = 'two.com';
        $this->assertEquals([
            new TestEntity(3, 'a'),
        ], $this->repo->getAll());
    }

    public function testLoadFromUnknownDomain()
    {
        $this->setDataInDb([
            'websites' => [
                ['id' => 1, 'name' => 'A', 'domain_name' => 'one.com'],
                ['id' => 2, 'name' => 'B', 'domain_name' => 'two.com'],
                ['id' => 3, 'name' => 'C', 'domain_name' => 'three.com'],
            ],
            'entities' => [
                ['id' => 1, 'unique_column' => 'a', 'website_id' => 1],
                ['id' => 2, 'unique_column' => 'b', 'website_id' => 1],
                ['id' => 3, 'unique_column' => 'a', 'website_id' => 2],
                ['id' => 4, 'unique_column' => 'c', 'website_id' => 3],
                ['id' => 5, 'unique_column' => 'd', 'website_id' => 3],
            ],
        ]);

        TestMultiSitePackage::$currentDomain = 'some-invalid-domain.com';
        $this->expectException(UnknownDomainException::class);
        $this->repo->getAll();
    }

    public function testDisabledEntityIgnoredDomain()
    {
        $repo = new TestDbDisabledEntityRepository($this->connection, $this->orm);

        $repo->save(new TestDisabledEntity());

        $this->assertDatabaseDataSameAs([
            'websites'          => [
                ['id' => 1, 'name' => 'A', 'domain_name' => 'one.com'],
                ['id' => 2, 'name' => 'B', 'domain_name' => 'two.com'],
                ['id' => 3, 'name' => 'C', 'domain_name' => 'three.com'],
            ],
            'disabled_entities' => [
                ['id' => 1],
            ],
        ]);

        $this->assertEquals([new TestDisabledEntity(1)], $repo->getAll());
    }
}